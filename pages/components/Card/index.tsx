import React from 'react'
import { InfoCardProps } from './card.interface'
import CardSection from './style'
import { getFormattedDate } from '../../../utils'

const Card = ({ school }: InfoCardProps) => {
  return (
    <CardSection>
      <h4>
        {school.degree} {school.stream} @ {school.schoolName}
      </h4>
      <p>
        {getFormattedDate(school.startYear)} - {getFormattedDate(school.endYear)}
      </p>
      <ul>
        <li>{school.degree}</li>
        <li>{school.description}</li>
      </ul>
    </CardSection>
  )
}

export default Card
