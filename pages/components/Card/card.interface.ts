export interface InfoCardProps {
  school: {
    schoolName: string
    degree: string
    stream: string
    startYear: string
    endYear: string
    grade: string
    description: string
  }
}
