import styled, { css } from 'styled-components'

const CardSection = styled.div`
  width: 40vw;
  height: 30vh;
  background-color: rgba(128, 128, 128, 0.5);
  margin: 2% auto;
  padding: 1rem;
`
export default CardSection
