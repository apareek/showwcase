import styled from 'styled-components'

export const AutoCompleteContainer = styled.ul`
  background: #fff;
  padding: 0px;
  list-style-type: none;
  position: absolute;
  top: 100%;
  left: 0;
  border: 1px solid #b6c1ce;
  border-radius: 2px;
  margin: 0px 25px;
  box-sizing: border-box;
  max-height: 280px;
  overflow-y: auto;
  z-index: 1;
`

export const AutoCompleteItem = styled.li`
  padding: 0px;
  width: 100%;
  box-sizing: border-box;
  &:hover {
    background-color: #ebf4ff;
  }
`

export const AutoCompleteItemButton = styled.button`
  line-height: 32px;
  font-size: 12px;
  background: white;
  text-align: left;
  border: none;
  width: 100%;
  &:active {
    outline: none;
    color: #0076f5;
  }
  &:hover {
    background-color: #ebf4ff;
  }
`

export const SuggestionInput = styled.div`
  position: relative;
  width: 100%;
`