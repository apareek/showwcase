import React, { useState, useCallback } from 'react'
import debounce from 'lodash/debounce'
import { getUniversityList } from '../../../services'
import { StyledSpan } from '../DateInput/style'
import { InfoCustomInputProps, ISuggestion } from './customInput.interface'
import {
  AutoCompleteContainer,
  AutoCompleteItem,
  AutoCompleteItemButton,
  SuggestionInput,
} from './style'
import Input from '../Input'

const CustomInput = ({ value, onChange, error }: InfoCustomInputProps) => {
  const [suggestions, setSuggestion] = useState<Array<ISuggestion>>([])
  const [isComponentVisible, setIsComponentVisible] = useState<boolean>(true)

  const searchForUniversityDebounce = useCallback(debounce((text) => searchForUniversity(text), 1000), []);

  const searchForUniversity = async (text: string) => {
    setSuggestion([])
    try {
      const universityList = await getUniversityList(text)
      setSuggestion(universityList)
      setIsComponentVisible(true)
    } catch (error) {
      console.error(error)
    }
  }

  const handleChangeApi = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target
    searchForUniversityDebounce(value)
    onChange(value)
  }

  const suggestionSelected = (value: ISuggestion) => {
    setIsComponentVisible(false)
    onChange(value.name)
  }

  return (
    <SuggestionInput>
      <SuggestionInput>
        <div onClick={() => setIsComponentVisible(false)} />
        <Input
          type='text'
          placeholder='School Name'
          name='schoolName'
          value={value}
          onChange={handleChangeApi}
        />
        {suggestions.length > 0 && isComponentVisible && (
          <AutoCompleteContainer>
            {suggestions.map((item: ISuggestion) => (
              <AutoCompleteItem key={item.code}>
                <AutoCompleteItemButton
                  key={item.code}
                  onClick={() => suggestionSelected(item)}
                >
                  {item.name}
                </AutoCompleteItemButton>
              </AutoCompleteItem>
            ))}
          </AutoCompleteContainer>
        )}
      </SuggestionInput>
      <StyledSpan>{error}</StyledSpan>
    </SuggestionInput>

  )
}

export default CustomInput
