import { Education } from "../../containers/MainScreen/mainScreen.interface";

export interface EducationInfoFormProps {
  addEducation: (education: Education) => void;
}
