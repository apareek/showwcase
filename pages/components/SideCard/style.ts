import styled from 'styled-components'

const SideCardSection = styled.div`
  width: 15vw;
  height: 71vh;
  background-color: rgba(128, 128, 128, 0.5);
  padding: auto;
  position: absolute;
  top: 12rem;
  left: 12rem;
`
export const List = styled.li`
  list-style-type: none;
  font-weight: bold;
  width: auto;
  margin-left: -1rem;
  margin-top: 1rem;
`
export default SideCardSection
