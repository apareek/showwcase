import React from 'react'
import { School, EducationList } from './sideCard.interface'
import SideCardSection, { List } from './style'

const SideCard = ({ educationList }: EducationList) => {
  return (
    <SideCardSection>
      <ul>
        {educationList.map((school: School, index: number) => (
          <List key={index}>{school.schoolName}</List>
        ))}
      </ul>
    </SideCardSection>
  )
}

export default SideCard
