export interface School {
  schoolName: string
  degree: string
  stream: string
  startYear: string
  endYear: string
  grade: string
  description: string
}

export interface EducationList {
  educationList: School[]
}
