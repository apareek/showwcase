export interface InfoInputProps {
  type: string
  placeholder: string
  name: string
  value: string
  error?: string
  onChange (e: React.ChangeEvent<HTMLInputElement>): void
}
