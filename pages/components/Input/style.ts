import styled from 'styled-components'

export const StyledInput = styled.input.attrs(() => ({
  size: 50,
}))`
  border-radius: 3px;
  border: 1px solid grey;
  display: block;
  margin-top: 2rem;
  padding: 15px;
  height:0.01rem;
  width: 85%;
  color: black;
  margin: 16px auto;

  ::placeholder {
    color: grey;
  }
`
export const StyledDiv = styled.div`
text-color: black;
`

