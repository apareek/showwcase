import React from 'react'
import { InfoDateProps } from './date.interface'
import { StyledDate, StyledLabel, StyledSpan, WrapperDiv } from './style'

const DateInput = (props: InfoDateProps) => {
  return (
    <div>
      <WrapperDiv>
        <StyledLabel>{props.label}</StyledLabel>
        <StyledDate
          {...props}
        />
      </WrapperDiv>
      <StyledSpan>{props.error}</StyledSpan>
    </div>
  )
}

export default DateInput
