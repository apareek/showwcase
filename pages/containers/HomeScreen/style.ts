import styled from 'styled-components'
const Container = styled.div`
  text-align: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 50vw;
  margin: 16vh auto;
  border: 2px solid grey;
  padding: 2rem;
`
export default Container
