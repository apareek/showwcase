import React, { useState } from 'react'
import Input from '../../components/Input'
import { useRouter } from 'next/router'
import Container from './style'
import { handleValidation } from '../../../utils'
import { IUtils } from '../../../utils.interface'

const HomeScreen = () => {
  const router = useRouter()

  const [name, setName] = useState<string>('')
  const [error, setError] = useState<string>('')
  
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value)
  }

  const submitName = (e: object) => {
    const errors: IUtils = handleValidation({ name })
    if (errors.name) {
      setError(errors.name)
      return
    }

    router.push({ pathname: `/main`, query: { name: name.trim() } })
    setName('')
  }
  
  return (
    <Container>
      <h4>Hi there! Welcome to your education showcase.</h4>
      <div>
        <p>Type your name And click `Enter` below to begin!</p>
        <Input
          type='text'
          placeholder='Your Name'
          name='name'
          value={name}
          onChange={handleChange}
          error={error}
        />
        <br />
        <button type='button' onClick={submitName}>
          Enter
        </button>
      </div>
    </Container>
  )
}

export default HomeScreen
