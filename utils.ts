import { IUtils } from "./utils.interface"

export const handleValidation = (fields: IUtils) => {
  let errorsObj: IUtils= {}

  if (!fields.schoolName) {
    errorsObj['schoolName'] = 'Cannot Be Empty'
  }
  if (!fields.degree) {
    errorsObj['degree'] = 'Cannot Be Empty'
  }
  if (fields['degree']) {
    if (!fields['degree'].match(/^[a-zA-Z]+$/)) {
      errorsObj['degree'] = 'Only letters'
    }
  }
  if (!fields.stream) {
    errorsObj['stream'] = 'Cannot Be Empty'
  }

  if (!fields.startYear) {
    errorsObj['startYear'] = 'Cannot Be Empty'
  }

  if (!fields.endYear) {
    errorsObj['endYear'] = 'Cannot Be Empty'
  }

  if (fields.startYear && fields.endYear) {
    const startYearTime = new Date(fields.startYear).getTime()
    const endYearTime = new Date(fields.endYear).getTime()

    const difference = startYearTime - endYearTime

    if (difference >= 0) {
      errorsObj['endYear'] = 'End Year should be greater than Start Year'
    }
  }

  if (!fields.grade) {
    errorsObj['grade'] = 'Cannot Be Empty'
  }
  if (fields['grade']) {
    if (!fields['grade'].match(/^[a-zA-Z]+$/)) {
      errorsObj['grade'] = 'Only letters'
    }
  }
  if (!fields.description) {
    errorsObj['description'] = 'Cannot Be Empty'
  }
  if (!fields.name) {
    errorsObj['name'] = 'Cannot Be Empty'
  }
  if (fields.name) {
    if (!fields.name.match(/^[a-zA-Z\s]*$/)) {
      errorsObj['name'] = 'Only letters'
    }
  }
  return errorsObj
}

export const getFormattedDate = (date: string): string => {
  const currentDate = new Date().getTime()
  const endDate = new Date(date).getTime()

  if (endDate > currentDate) {
    return 'Present'
  }

  return new Date(date).toLocaleDateString("en-US", { year: 'numeric', month: 'long' })
}