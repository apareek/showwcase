export interface IUtils {
    schoolName?: string
    degree?: string
    stream?: string
    startYear?: string
    endYear?: string
    grade?: string
    description?: string,
    name?: string
}